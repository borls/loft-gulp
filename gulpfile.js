"use strict";

var gulp = require('gulp'),
	uncss = require('gulp-uncss'),
	concatCSS = require('gulp-concat-css'),
 	rename = require('gulp-rename'),
 	notify = require('gulp-notify'),
 	prefix = require('gulp-autoprefixer'),
 	livereload = require('gulp-livereload'),
 	connect = require('gulp-connect'),
 	sass = require('gulp-sass'),
 	minifyCSS = require('gulp-minify-css');
 

// server connect
gulp.task('connect', function() {
  connect.server({
    root: 'app',
    livereload: true
  });
});

// css
gulp.task('css', function () {
  gulp.src('css/*.css')
    .pipe(concatCSS('bundle.css'))
    // .pipe(sass())
    .pipe(minifyCSS())
    .pipe(rename('bundle.min.css'))
    .pipe(prefix({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(livereload())
    .pipe(gulp.dest('app/css'))
    .pipe(notify('Done!'))
    .pipe(connect.reload());
});
// scss
gulp.task('scss', function () {
  gulp.src('scss/*.scss')
    // .pipe(concatCSS('bundle.css'))
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(rename('bundle.min.css'))
    .pipe(prefix({
            browsers: ['last 15 versions'],
            cascade: false
        }))
    .pipe(livereload())
    .pipe(gulp.dest('app/css'))
    // .pipe(notify('Done!'));
    .pipe(connect.reload());
});

// uncss
gulp.task('uncss', function () {
  gulp.src('css/common.css')
    .pipe(uncss({
    	html: ['app/index.html']
    }))
    .pipe(gulp.dest('app/css'))
    // .pipe(connect.reload());
});

// uncss bootstrap
gulp.task('bootstrap', function () {
  gulp.src('bower_components/bootstrap/dist/css/bootstrap.css')
    .pipe(uncss({
    	html: ['app/index.html']
    }))
    .pipe(gulp.dest('app/css'))
    // .pipe(connect.reload());
});

// html
gulp.task('html', function () {
	gulp.src('app/index.html')
	.pipe(connect.reload());
})

// watch
gulp.task('watch', function () {
	gulp.watch('css/*.css', ['css'])
	gulp.watch('app/index.html', ['html'])
})

// default
gulp.task('default', ['connect', 'html', 'css', 'watch']);